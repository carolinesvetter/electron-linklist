const { app, shell, BrowserWindow, Menu, ipcMain } = require("electron");
const path = require("path");
const isDev = require("electron-is-dev");

var knex = require("knex")({
  client: "sqlite3",
  connection: {
    filename: path.join("./assets/linklist.sqlite"),
  },
  useNullAsDefault: true,
});

// set up window
function createWindow() {
  // Eset up browser window
  const mainWindow = new BrowserWindow({
    width: 800,
    height: 600,
    webPreferences: {
      nodeIntegration: true,
    },
  });

  // and load the index.html of the app.
  mainWindow.loadURL(
    isDev
      ? "http://localhost:3000"
      : `file://${path.join(__dirname, "index.html")}`
  );

  // set up menu template
  const mainMenuTemplate = [
    // Each object is a dropdown
    {
      label: "File",
      submenu: [
        {
          label: "Quit",
          accelerator: process.platform === "darwin" ? "Command+Q" : "Ctrl+Q",
          click() {
            app.quit();
          },
        },
      ],
    },
  ];

  // add dev options
  if (isDev) {
    mainMenuTemplate.push({
      label: "Developer Tools",
      submenu: [
        {
          role: "reload",
        },
        {
          label: "Toggle DevTools",
          accelerator: process.platform === "darwin" ? "Command+I" : "F12",
          click(item, focusedWindow) {
            focusedWindow.toggleDevTools();
          },
        },
      ],
    });
  }

  // unshift empty array to avoid empty pixel on mac
  if (process.platform === "darwin") {
    mainMenuTemplate.unshift({});
  }

  // Build menu from template
  const mainMenu = Menu.buildFromTemplate(mainMenuTemplate);

  // Insert menu
  Menu.setApplicationMenu(mainMenu);

  // make sure all Links with target="_blank" are opened in an external browser.
  mainWindow.webContents.on("new-window", function (event, url) {
    event.preventDefault();
    shell.openExternal(url);
  });

  // Handle Catch On Main Event.
  ipcMain.on("fetch-links-from-db", (event, args) => {
    knex
      .select("*")
      .from("links")
      .orderBy("id", "desc")
      .then(function (rows) {
        const data = unStringifyTags(rows);
        mainWindow.send("return-fetched-links", data);
      });
  });

  // Parse the tags of a link
  function unStringifyTags(data) {
    const newData = [];
    data.forEach((element) => {
      if (
        typeof element.tags === "string" &&
        element.tags !== "null" &&
        element.tags !== "undefined"
      )
        element.tags = JSON.parse(element.tags);
      else element.tags = [];
      newData.push(element);
    });
    return newData;
  }

  // Handle Add Link to DB Event.
  ipcMain.handle("add-link", (event, arg) => {
    const jsonTags = JSON.stringify(arg.tags);
    knex("links")
      .insert({
        title: arg.title,
        description: arg.description,
        url: arg.url,
        tags: jsonTags,
      })
      .then(function (result) {});
  });
}
app.whenReady().then(createWindow);

// Quit when all windows are closed.
app.on("window-all-closed", () => {
  if (process.platform !== "darwin") {
    app.quit();
  }
});

// On Activate open a new window.
app.on("activate", () => {
  if (BrowserWindow.getAllWindows().length === 0) {
    createWindow();
  }
});
