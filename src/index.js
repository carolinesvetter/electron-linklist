import React from "react";
import ReactDOM from "react-dom";
import "./css/reset.css";
import "./css/app.css";
import "semantic-ui-css/semantic.min.css";
import { HashRouter as Router, Route } from "react-router-dom";

import Home from "./App";

ReactDOM.render(
  <Router>
    <div>
      <main>
        <Route exact path="/" component={Home} />
      </main>
    </div>
  </Router>,
  document.getElementById("root")
);
