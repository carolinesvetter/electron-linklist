import React, { PureComponent } from "react";
import { Label } from "semantic-ui-react";

/**
 * Tag class
 */
class Tag extends PureComponent {
  /**
   * onClick handler for the tag
   */
  tagClicked(event, values) {
    this.props.addKeyword(values.content);
  }
  /**
   * render method
   */
  render() {
    return (
      <Label
        as="a"
        onClick={this.tagClicked.bind(this)}
        content={this.props.tagData.name}
        icon="tag"
        key={this.props.tagData.name}
      />
    );
  }
}

export default Tag;
