import React, { PureComponent } from "react";
import { Divider, Header, Icon, Table, Input } from "semantic-ui-react";
import Link from "./Link";
import AddLink from "./AddLink";
import "./../css/reset.css";
import "./../css/app.css";

const electron = window.require("electron");
const ipcRenderer = electron.ipcRenderer;

/**
 * Linklist Class
 */
class Linklist extends PureComponent {
  /**
   * @var keyword : String
   */
  keyword = "";

  /**
   * @var initialList : Array
   */
  initialList = [];

  /**
   * @var currentList : Array
   */
  currentList = [];

  /**
   * @var index : int
   */
  index = [];

  /**
   * state
   */
  state = {
    version: 0,
  };

  /**
   * updates state version
   */
  updateState() {
    this.setState({ version: this.state.version + 1 });
  }

  /**
   * updates the keyword, filteres the list and updates the state after it
   */
  updateKeyword(newKeyword) {
    this.keyword = newKeyword;
    this.filterList();
    this.updateState();
  }

  /**
   * adds a keyword to the current keyword
   */
  addKeyword(toAdd) {
    // if the current keyword is undefined or just an empty string, fill it empty or take the current keyword and add the new after it
    var newKeyword =
      (this.keyword === undefined || this.keyword === ""
        ? ""
        : this.keyword + " ") + toAdd;
    this.updateKeyword(newKeyword);
  }

  /**
   * checks the tags for a match with the keyword
   * @param {*} tags              all tags of the current link
   * @param {String} keyword      current keyword
   * @returns {boolean} isMatch   result of, if the keyword matches at least one tag
   */
  checkTags(tags, keyword) {
    var isMatch = false;
    tags.map((tag) => {
      // if the tag.name matches the keyword
      if (tag.name.toLowerCase().includes(keyword.toLowerCase())) {
        isMatch = true;
      }
      return isMatch;
    });
    return isMatch;
  }
  /**
   * splits the keySequenz String into an array without commas
   * @param {*} keySequenz
   */
  splitKeywords(keySequenz) {
    let keyArray = [];
    let newKeySequence = keySequenz;

    // TODO replace if there is 2+ space side by side
    newKeySequence = newKeySequence.replace(" , ", " ");
    newKeySequence = newKeySequence.replace(", ", " ");
    newKeySequence = newKeySequence.replace(",", " ");
    keyArray = newKeySequence.split(" ");

    return keyArray;
  }
  /**
   * handles the keyword change event of the search input
   * @param {*} event
   * @param {*} values
   */
  handleKeywordChange(event, values) {
    this.keyword = values.value;
    this.filterList();
    this.updateState();
  }
  /**
   * filters the list based on keyword
   */
  filterList() {
    var initialList = [];
    var newList = [];
    var temp = [];
    // TODO split keywords / for each keyword
    var keywords = this.splitKeywords(this.keyword.toLowerCase());
    // if there is more than one keyword
    if (keywords.length > 1) {
      initialList = this.initialList;
      // run through all keywords
      keywords.forEach((keyword) => {
        // run through all items of the initial list
        initialList.map((item) => {
          var filter = keyword;
          var title = item.title.toLowerCase().includes(filter);
          var tags = this.checkTags(item.tags, filter);
          var description = item.description.toLowerCase().includes(filter);
          // if any of the keywords fits with the given data, put it into the temp array
          if (title || tags || description) {
            temp.push(item);
          }
          return title || tags || description;
        });
      });
      // filter the temp array to exclude unique items → Only the duplicates shall be shown in the currentList
      newList = temp.filter(
        (a, i, aa) => aa.indexOf(a) === i && aa.lastIndexOf(a) !== i
      );
    } else {
      // if the input field is filled, filter the initial list based on the given keyword
      if (this.keyword !== "") {
        initialList = this.initialList;
        //filter the initial list based on the given keyword
        newList = initialList.filter((item) => {
          var filter = this.keyword.toLowerCase();
          var title = item.title.toLowerCase().includes(filter);
          var tags = this.checkTags(item.tags, filter);
          var description = item.description.toLowerCase().includes(filter);
          return title || tags || description;
        });
      } else {
        newList = this.initialList;
      }
    }
    this.currentList = newList;
  }
  /**
   * handles the addition of a new list item
   * TODO send to API
   */
  handleAddLinkSubmit(formData) {
    this.index++;
    this.initialList.unshift(formData);
    ipcRenderer.invoke("add-link", formData).then((result) => {
      console.log(result);
    });
    this.currentList = this.initialList;
    this.updateState();
  }
  /**
   * load in initial link-list
   */
  componentDidMount() {
    ipcRenderer.send("fetch-links-from-db", null);
    ipcRenderer.on("return-fetched-links", (e, v) => {
      this.currentList = v;
      this.initialList = v;
      this.index = v.length;
      this.updateState();
    });
  }
  componentWillUnmount() {
    ipcRenderer.removeListener("fetch-links-from-db", this.handleData);
    ipcRenderer.removeListener("pong", this.updateState);
  }
  /**
   * render method
   */
  render() {
    return (
      <div>
        <Divider horizontal>
          <Header as="h4">
            <Icon name="add" />
            Add Link
          </Header>
        </Divider>
        <AddLink
          handleAddLinkSubmit={this.handleAddLinkSubmit.bind(this)}
          nextIndex={this.index + 1}
        />
        <Divider horizontal>
          <Header as="h4">
            <Icon name="search" />
            Search
          </Header>
        </Divider>
        <Input
          className="m-5"
          fluid
          icon={{ name: "search", circular: true, link: true }}
          placeholder="Search..."
          value={this.keyword}
          onChange={this.handleKeywordChange.bind(this)}
        />
        <Divider horizontal>
          <Header as="h4">
            <Icon name="linkify" />
            Linklist
          </Header>
        </Divider>
        <Table>
          <Table.Header>
            <Table.Row>
              <Table.HeaderCell>Name</Table.HeaderCell>
              <Table.HeaderCell>Description</Table.HeaderCell>
              <Table.HeaderCell>Tags</Table.HeaderCell>
            </Table.Row>
          </Table.Header>
          <Table.Body>
            {this.currentList.map((link) => (
              <Link
                linkdata={link}
                addKeyword={this.addKeyword.bind(this)}
                key={link.id}
              />
            ))}
          </Table.Body>
        </Table>
      </div>
    );
  }
}

export default Linklist;
