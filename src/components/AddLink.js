import React, { PureComponent } from "react";
import { Form, Container, Button, Input } from "semantic-ui-react";

/**
 * AddLink Class
 */
class AddLink extends PureComponent {
  title = "";
  url = "";
  description = "";
  tags = "";
  /**
   * state
   */
  state = {
    version: 0,
  };
  /**
   * handles the change event of a input title
   * @param {*} event
   * @param {*} values
   */
  handleInputChangeTitle(event, values) {
    this.title = values.value;
    this.updateState();
  }
  /**
   * handles the change event of a input URL
   * @param {*} event
   * @param {*} values
   */
  handleInputChangeURL(event, values) {
    this.url = values.value;
    this.updateState();
  }

  /**
   * handles the change event of a input description
   * @param {*} event
   * @param {*} values
   */
  handleInputChangeDescription(event, values) {
    this.description = values.value;
    this.updateState();
  }
  /**
   * handles the change event of a input tags
   * @param {*} event
   * @param {*} values
   */
  handleInputChangeTags(event, values) {
    this.tags = values.value;
    this.updateState();
  }
  /**
   * adds the tags into a JSON format
   */
  tagsToJSON() {
    var tagArray = [];
    console.log("[" + this.tags + "]");
    tagArray = this.tags.split(",");
    var tagsJSON = [];
    tagArray.forEach((tag) => {
      var tagJSON = {
        name: tag.trim(),
      };
      tagsJSON.push(tagJSON);
    });
    return tagsJSON;
  }
  /**
   * handles the submit action of the form
   */
  handleAddLinkSubmit(event, values) {
    event.preventDefault();
    var newEntry = {
      id: this.props.nextIndex,
      title: this.title,
      description: this.description,
      url: this.url,
      tags: this.tagsToJSON(),
    };
    this.props.handleAddLinkSubmit(newEntry);
    this.title = "";
    this.url = "";
    this.description = "";
    this.tags = "";
    this.updateState();
  }
  /**
   * updates state version
   */
  updateState() {
    this.setState({ version: this.state.version + 1 });
  }
  /**
   * render method
   */
  render() {
    return (
      <Container>
        <Form onSubmit={this.handleAddLinkSubmit.bind(this)}>
          <Form.Group widths="2">
            <Form.Field>
              <Input
                placeholder="Title"
                value={this.title}
                name="title"
                id="title"
                onChange={this.handleInputChangeTitle.bind(this)}
              />
            </Form.Field>
            <Form.Field>
              <Input
                placeholder="http://"
                value={this.url}
                name="url"
                onChange={this.handleInputChangeURL.bind(this)}
              />
            </Form.Field>
            <Form.Field>
              <Input
                placeholder="Description"
                value={this.description}
                name="description"
                onChange={this.handleInputChangeDescription.bind(this)}
              />
            </Form.Field>
            <Form.Field>
              <Input
                placeholder="PHP, MySQL, ..."
                value={this.tags}
                name="tags"
                onChange={this.handleInputChangeTags.bind(this)}
              />
            </Form.Field>
          </Form.Group>
          <Button fluid primary type="submit">
            Submit
          </Button>
        </Form>
      </Container>
    );
  }
}

export default AddLink;
