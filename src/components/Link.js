import React, { PureComponent } from "react";
import { Table } from "semantic-ui-react";
import Tag from "./Tag";

/**
 * Link Class
 */
class Link extends PureComponent {
  /**
   * render method
   */
  render() {
    return (
      <Table.Row key={this.props.linkdata.id}>
        <Table.Cell>
          <a
            href={this.props.linkdata.url}
            target="_blank"
            rel="noopener noreferrer"
          >
            {this.props.linkdata.title}
          </a>
        </Table.Cell>
        <Table.Cell>{this.props.linkdata.description}</Table.Cell>
        <Table.Cell>
          {this.props.linkdata.tags.map((tag) => (
            <Tag
              tagData={tag}
              addKeyword={this.props.addKeyword}
              key={tag.name}
            />
          ))}
        </Table.Cell>
      </Table.Row>
    );
  }
}

export default Link;
