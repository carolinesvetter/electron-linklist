import React from "react";
import Linklist from "./components/Linklist";
import "./css/reset.css";
import "./css/app.css";
import { Divider } from "semantic-ui-react";

function App() {
  return (
    <div className="App">
      <Linklist />
      <Divider />
      <ul>
        <li>
          Application Icon from{" "}
          <a
            target="_blank"
            rel="noopener noreferrer"
            href="http://icons8.com/"
          >
            Icons8.com
          </a>
        </li>
      </ul>
    </div>
  );
}

export default App;
